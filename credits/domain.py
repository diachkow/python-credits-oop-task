"""This module contains classes that represents domain entities."""


import datetime
from dataclasses import dataclass
from decimal import Decimal
from typing import Optional

from .errors import TrancheInvestmentError
from ._base import Subscriber
from .helpers import MonthProfitCalculator


class Investor:
    def __init__(
        self,
        name: str,
        profit: Optional[Decimal] = None,
        account: Optional[Decimal] = None,
    ):
        self.name = name
        self.profit = profit or Decimal('0.0')

        self._account = account or Decimal('0.0')

    def __str__(self) -> str:
        return (
            f'<Investor: name={self.name}, account={self._account}, '
            f'profit={self._profit}'
        )

    def __repr__(self):
        return self.__str__()

    @property
    def account(self) -> Decimal:
        return self._account

    def withdraw(self, amount: Decimal):
        if self._account < amount:
            raise ValueError('You can not withdraw more than {self._account}')
        self._account -= amount


@dataclass
class Investment:
    investor: Investor
    amount: Decimal
    date: datetime.date


class Tranche:
    def __init__(
        self,
        *,
        name: str,
        rate: float,
        capacity: Decimal,
        credit: Optional['Credit'] = None
    ):
        self.credit = credit

        self._name = name
        self._rate = rate
        self._capacity = capacity

        self._investments: list[Investment] = []

    def __str__(self) -> str:
        return f'<Tranche: {self._name}>'

    @property
    def total_invested(self) -> Decimal:
        return sum(investment.amount for investment in self._investments)

    @property
    def investments(self) -> list[Investment]:
        return self._investments.copy()

    @property
    def rate(self) -> float:
        return self._rate

    def invest(self, investment: Investment):
        if investment.amount > investment.investor.account:
            raise TrancheInvestmentError(
                'Not enough money on investor account')

        if not self._check_investment_is_in_credit_date_range(investment):
            raise TrancheInvestmentError(
                'Investment date is not in credit dates.')

        if self.total_invested + investment.amount <= self._capacity:
            investment.investor.withdraw(investment.amount)
            self._investments.append(investment)
        else:
            max_investment = max(0, self._capacity - self.total_invested)
            raise TrancheInvestmentError(
                'You can not invest in this tranche more. Maximum available '
                f'investment is {max_investment}')

    def _check_investment_is_in_credit_date_range(
        self,
        investment: Investment
    ) -> bool:
        if self.credit is None:
            return True
        return self.credit.start_date <= investment.date \
            <= self.credit.end_date


class Credit(Subscriber):
    def __init__(
        self,
        start_date: datetime.date,
        end_date: datetime.date
    ):
        self._start_date = start_date
        self._end_date = end_date

        self._tranches: list[Tranche] = []

    @property
    def tranches(self) -> list[Tranche]:
        return self._tranches.copy()

    @property
    def start_date(self) -> datetime.date:
        return self._start_date

    @property
    def end_date(self) -> datetime.date:
        return self._end_date

    def add_tranche(self, tranche: Tranche):
        tranche.credit = self
        self._tranches.append(tranche)

    def trigger(self, date: datetime.date):
        if date.day != 1:
            return

        calculator = MonthProfitCalculator(date)

        for tranche in self._tranches:
            for investment in tranche.investments:
                investment.investor.profit += calculator.calculate(
                    rate=tranche.rate,
                    investment=investment
                )


class InvestmentIntent(Subscriber):
    """
    This class represents the desire to made an investment to certain
    tranche. When the ``investment.date`` is coming, this class will try to do
    an investment (call ``tranche.invest``).

    Check out ``.succeeded`` property to see the investment status.
    """

    def __init__(self, tranche: Tranche, investment: Investment):
        self._tranche = tranche
        self._investment = investment

        self._succeeded: Optional[bool] = None

    def __str__(self) -> str:
        return f'<InvestmentIntent: {self._investment} -> {self._tranche}>'

    @property
    def succeeded(self) -> bool:
        return bool(self._succeeded)

    def trigger(self, date: datetime.date):
        if self._succeeded is None and self._investment.date == date:
            try:
                self._tranche.invest(self._investment)
            except TrancheInvestmentError:
                self._succeeded = False
            else:
                self._succeeded = True
