class TrancheInvestmentError(Exception):
    """Raised when there is any error with investing into tranche."""
