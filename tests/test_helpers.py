from datetime import date
from decimal import Decimal

import pytest

from credits import Investor, Investment
from credits.helpers import (
    DateHelper,
    PercentCalculator, MonthProfitCalculator
)


def test_get_previous_month_first_day():
    result = DateHelper.get_previous_month_first_day(date(2020, 11, 1))

    assert result == date(2020, 10, 1)


@pytest.mark.parametrize('start_date,end_date,expected', [
    (date(2015, 10, 15), date(2015, 10, 25), 10),
    (date(2015, 10, 15), date(2015, 11, 15), 31),
])
def test_get_dates_delta_in_days(
    start_date: date,
    end_date: date,
    expected: int
):
    result = DateHelper.get_dates_delta_in_days(end_date, start_date)

    assert result == expected


class TestPercentCalculator:
    def test_get_percents_x_of_y(self):
        result = PercentCalculator.get_percents_x_of_y(28, 31)

        assert round(result, 2) == 90.32

    def test_get_x_percents_of_y(self):
        result = PercentCalculator.get_x_percents_of_y(50, 30)

        assert result == 15


def test_month_profit_calculator():
    calculator = MonthProfitCalculator(date(2020, 11, 1))
    investor = Investor(name='Investor')
    investment = Investment(investor=investor,
                            amount=Decimal('500.00'),
                            date=date(2020, 10, 10))

    result = calculator.calculate(rate=6.0, investment=investment)

    assert result == Decimal('21.29')
