import datetime

from decimal import Decimal

import pytest

from credits import Tranche, Investor, Investment
from credits.domain import Credit, InvestmentIntent
from credits.errors import TrancheInvestmentError


class TestTranche:
    def test_tranche_invest(self, tranche: Tranche, investor: Investor):
        investment = Investment(
            investor=investor,
            amount=Decimal('568.0'),
            date=datetime.date(2015, 10, 15)
        )

        tranche.invest(investment)

        assert tranche.total_invested == Decimal('568.0')
        assert investor.account == Decimal('9432.00')

    def test_tranche_capacity_overflowed(
        self,
        tranche: Tranche,
        investor: Investor
    ):
        investment = Investment(
            investor=investor,
            amount=Decimal('1000.00'),
            date=datetime.date(2020, 10, 15)
        )
        tranche.invest(investment)
        invalid_investment = Investment(
            investor=investor,
            amount=Decimal('1.00'),
            date=datetime.date(2020, 10, 16)
        )

        with pytest.raises(TrancheInvestmentError):
            tranche.invest(invalid_investment)

    def test_tranche_investment_date_not_in_credit_range(
        self,
        tranche: Tranche,
        investor: Investor
    ):
        credit = Credit(
            start_date=datetime.date(2020, 10, 1),
            end_date=datetime.date(2020, 11, 1)
        )
        credit.add_tranche(tranche)
        investment = Investment(
            investor=investor,
            amount=Decimal('1200.00'),
            date=datetime.date(2020, 12, 1)
        )

        with pytest.raises(TrancheInvestmentError) as exc_info:
            tranche.invest(investment)

        assert 'Investment date is not in credit dates.' in str(exc_info.value)


class TestInvestor:
    def test_investor_withdraw(self, investor: Investor):
        investor.withdraw(Decimal('20.00'))

        assert investor.account == Decimal('9980.00')

    def test_investor_withdraw_not_enough_money(self, investor: Investor):
        with pytest.raises(ValueError):
            investor.withdraw(Decimal('1000000.00'))


class TestCredit:
    def test_credit_add_tranche(self, credit: Credit, tranche: Tranche):
        credit.add_tranche(tranche)

        assert tranche in credit.tranches
        assert tranche.credit is credit

    def test_credit_calculate_profit(self, credit: Credit, tranche, investor):
        credit.add_tranche(tranche)

        investment = Investment(
            investor=investor,
            amount=Decimal('1000.0'),
            date=datetime.date(2015, 10, 3)
        )

        tranche.invest(investment)

        credit.trigger(datetime.date(2015, 11, 1))

        assert investor.profit == Decimal('28.06')
        assert investor.account


class TestAddingInvestmentByDate:
    def test_adding_investment_to_tranche_by_date(
        self,
        tranche: Tranche,
        investment: Investment
    ):
        intent = InvestmentIntent(tranche=tranche, investment=investment)

        intent.trigger(date=datetime.date(2020, 10, 15))

        assert investment in tranche.investments
        assert intent.succeeded

    def test_adding_investment_to_tranche_by_date_still_not_processed(
        self,
        tranche: Tranche,
        investment: Investment
    ):
        intent = InvestmentIntent(tranche=tranche, investment=investment)

        intent.trigger(date=datetime.date(2020, 10, 14))

        assert investment not in tranche.investments
        assert not intent.succeeded

    def test_investment_intent_failed(
        self,
        tranche: Tranche,
        investor: Investor
    ):
        investment = Investment(
            investor=investor,
            amount=Decimal('1200.00'),
            date=datetime.date(2020, 10, 15)
        )
        intent = InvestmentIntent(tranche=tranche, investment=investment)

        intent.trigger(date=datetime.date(2020, 10, 15))

        assert investment not in tranche.investments
        assert not intent.succeeded
